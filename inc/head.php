<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Boîte à outils - EPN Relais 59</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<style>body{padding-top: 2rem;}</style>
  </head>

  <body>

    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="">Relais 59</a>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="?page=accueil">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://www.csrelais59.org/epn" target="_blank">Le site vitrine</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://gitlab.com/relais59" target="_blank">Notre Gitlab</a>
          </li>
<!-- au cas où
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Désactivé</a>
          </li>
-->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Notre présence web</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" target="_blank" href="https://www.facebook.com/relais.59">Facebook</a>
              <a class="dropdown-item" target="_blank" href="https://twitter.com/relais59">Twitter</a>
              <a class="dropdown-item" target="_blank" href="https://www.instagram.com/relais59">Instagram</a>
              <a class="dropdown-item" target="_blank" href="https://steamcommunity.com/groups/relais59">Steam</a>
              <a class="dropdown-item" target="_blank" href="http://dia.so/relais59">Diaspora</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
