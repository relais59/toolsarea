<?php
ini_set('display_errors', 'On');
/* 
 *
 * Index de l'accueil de l'accès web au serveur. Inclu head, header et footer. $
 *
*/


include 'inc/head.php';

/*
On switch les pages ici. Si pages rajoutées, penser à les intégrer dans ce s$
*/
if (! isset($_GET['page'])) {
        include'pages/accueil.html';
}
else {
        $page = $_GET['page'];
        switch($page) {
                case 'accueil':
                include 'pages/accueil.html';
                break;
                case 'contact':
                include 'pages/form.php';
                break;
        }
}

include 'inc/footer.php';

?>

